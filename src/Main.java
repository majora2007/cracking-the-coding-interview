import com.majora2007.ctci.IntWrapper;
import com.majora2007.ctci.graphs.BinarySearchTree;
import com.majora2007.ctci.graphs.BSTNode;
import com.majora2007.ctci.graphs.game.Game;

/**
 * User: jvmilazz
 * Date: 2/4/14
 * Time: 9:02 AM
 */
public class Main {


    public static void main(String[] args)
    {
        //Chapter1Application chapter1App = new Chapter1Application();
        //System.out.println(chapter1App.execute());

        /*IntWrapper wrapper = new IntWrapper(0);
        System.out.println("wrapper original value =  " + wrapper.data);

        incrementInt(wrapper);
        System.out.println("void incrementInt(object) =  " + wrapper.data);
        IntWrapper wrapper2 = incrementIntWithReturn(wrapper);
        System.out.println("After incrementIntWithReturn, wrapper1 =  " + wrapper.data);
        System.out.println("After incrementIntWithReturn, wrapper2 =  " + wrapper2.data);
        System.out.println("After incrementIntWithReturn, wrapper1 == wrapper2?  " + wrapper.equals(wrapper2));

        incrementIntRecursively(wrapper);
        System.out.println("Increment Recursively " + wrapper.data);

        runBST();*/

        Game mazeGame = new Game();
        mazeGame.display();

    }

    public static void runBST()
    {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        BSTNode root = new BSTNode(8);

        binarySearchTree.insert(root, new BSTNode(3));
        binarySearchTree.insert(root, new BSTNode(10));
        binarySearchTree.insert(root, new BSTNode(1));
        binarySearchTree.insert(root, new BSTNode(6));
        binarySearchTree.insert(root, new BSTNode(4));
        binarySearchTree.insert(root, new BSTNode(7));
        binarySearchTree.insert(root, new BSTNode(13));
        binarySearchTree.insert(root, new BSTNode(14));

        System.out.println("In Order Traversal: ");
        binarySearchTree.inOrderTraversal(root);

        System.out.println("Pre Order Traversal: ");
        binarySearchTree.preOrderTraversal(root);

        System.out.println("Post Order Traversal: ");
        binarySearchTree.postOrderTraversal(root);

        System.out.println("Find 7: ");
        boolean result = binarySearchTree.binarySearch(root, new BSTNode(7));
        System.out.println("BinarySearchTree contains 7? " + result);



    }

    public static void incrementIntRecursively(IntWrapper object)
    {
        if (object.data == 10) return;
        object.data++;
        incrementIntRecursively(object);
    }

    public static void incrementInt(IntWrapper object)
    {
        object.data++;
    }

    public static IntWrapper incrementIntWithReturn(IntWrapper object)
    {
        object.data++;
        return object;
    }

    public static void incrementPrimative(int data)
    {
        data++;
    }

    public static int incrementPrimativeWithReturn(int data)
    {
        data++;
        return data;
    }
}
