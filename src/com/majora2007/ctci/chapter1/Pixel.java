package com.majora2007.ctci.chapter1;

/**
 * User: jvmilazz
 * Date: 2/4/14
 * Time: 12:42 PM
 */
public class Pixel {
    private byte red, green, blue, alpha;

    public Pixel() {
        red = 0;
        green = 0;
        blue = 0;
        alpha = 127;
    }

    public Pixel(byte red, byte green, byte blue, byte alpha)
    {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    /**
     * NOTE: This is only here because I know I am only to be given bytes.
     * @param red
     * @param green
     * @param blue
     * @param alpha
     */
    public Pixel(final int red, final int green, final int blue, final int alpha) {
        this.red = (byte) red;
        this.green = (byte) green;
        this.blue = (byte) blue;
        this.alpha = (byte) alpha;
    }

    public byte getRed() {
        return red;
    }

    public void setRed(final byte red) {
        this.red = red;
    }

    public byte getGreen() {
        return green;
    }

    public void setGreen(final byte green) {
        this.green = green;
    }

    public byte getBlue() {
        return blue;
    }

    public void setBlue(final byte blue) {
        this.blue = blue;
    }

    public byte getAlpha() {
        return alpha;
    }

    public void setAlpha(final byte alpha) {
        this.alpha = alpha;
    }

    @Override
    public String toString() {
        return "(" +
                red +
                "," + green +
                "," + blue +
                "," + alpha +
                ')';
    }
}
