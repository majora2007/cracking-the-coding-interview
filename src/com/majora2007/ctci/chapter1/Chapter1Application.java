package com.majora2007.ctci.chapter1;

import java.util.Arrays;
import java.util.HashMap;

/**
 * User: jvmilazz
 * Date: 2/4/14
 * Time: 9:01 AM
 */
public class Chapter1Application {

    private StringBuilder resultBuilder = new StringBuilder();

    public Chapter1Application() {}

    public String execute() {

        resultBuilder.append("Chapter 1 Questions:\n");

        String input = "abcdefg";
        resultBuilder.append("hasUniqueCharactersWithHash(" + input + "):\t");
        resultBuilder.append(hasUniqueCharactersWithHash(input));
        resultBuilder.append("\n");
        return resultBuilder.toString();
    }

    /**
     * Implement an algorithm to determine if a string has all unique characters.
     *
     * This implementation uses a hashmap (no synchronization neccessary).
     * @param str
     * @return
     */
    public boolean hasUniqueCharactersWithHash(String str)
    {
        HashMap characterMap = new HashMap<Character, Integer>();
        char[] inputString = str.toCharArray();

        for(char c : inputString)
        {
            if (characterMap.containsKey(new Character(c))) {
                return false;
            } else {
                characterMap.put(new Character(c), 1);
            }
        }

        return true;
    }

    /**
     * Implement an algorithm to determine if a string has all unique characters.
     * @param str
     * @return
     */
    public boolean hasUniqueCharacters(String str)
    {
        // Sort characters using mergesort: O(n log n)
        char[] input = str.toCharArray();
        Arrays.sort(input);

        // If there are two of same characters in a row, return false
        for (int i = 0; i < input.length - 1; i++)
        {
            char currentChar = input[i];
            if (currentChar == input[i + 1]) return false;
        }

        return true;
    }

    /**
     * Given two strings, write a method to decide if one is a permutation of the other.
     *
     * To be a permutation, both strings must have same number of unique characters.
     *
     * @param str1
     * @param str2
     * @return
     */
    public boolean isPermutationOf(String str1, String str2)
    {
        if (str1.length() != str2.length()) return false;

        char[] array1 = str1.toCharArray();
        char[] array2 = str2.toCharArray();

        // Merge sort arrays
        Mergesort mergesort = new Mergesort();
        mergesort.sort(array1);
        mergesort.sort(array2);

        if (new String(array1).equals(new String(array2)))
        {
            return true;
        }
        return false;
    }

    /**
     * Implement a method to perform basic string compression using the counts
     * of repeated characters. For example, the string 'aabcccccaaa' would
     * become 'a2b1c5a3'. If the compressed string would not become smaller
     * than the original string, your method should return the original string.
     *
     * Assumptions: I am assuming the strings that are being compressed are not
     * long. If they are long sequences, my solution would be Huffman Codes.
     *
     * @param uncompressedString
     * @return
     */
    public String compressString(String uncompressedString)
    {
        StringBuffer compressedString = new StringBuffer(uncompressedString.length());
        char[] uncompressed = uncompressedString.toCharArray();
        for (int i = 0; i < uncompressed.length; )
        {
            int counter = 1;
            char currentChar = uncompressed[i];
            for (int j = i + 1; j < uncompressed.length; j++)
            {
                if (uncompressed[j] == currentChar) {
                    counter++;
                } else {
                    break;
                }
            }

            compressedString.append(currentChar);
            compressedString.append(counter);
            i += counter;
            counter = 0;
        }

        String compressed = compressedString.toString();
        if (compressed.length() < uncompressedString.length())
        {
            return compressed;
        } else {
            return uncompressed.toString();
        }
    }

    /**
     * Given an image represented by an NxN matrix, where each pixel in the image is 4 bytes,
     * write a method to rotate the image by 90 degrees. Can you do this in place?
     *
     * Rotate using layers
     * @param originalImage
     */
    public void rotateImageBy90Degrees(Pixel[][] originalImage)
    {
        /*int N = originalImage.length - 1;
        int layer = 0;
        int j = 0;

        for (int i = 0; i < originalImage.length - 1; i++)
        {
            Pixel temp = originalImage[i][N - j];
            originalImage[i][N-j] = originalImage[j][i];
            originalImage[j][i] = temp;
        }*/
        throw new RuntimeException("Not Implemented.");

    }
}
