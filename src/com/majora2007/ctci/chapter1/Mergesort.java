package com.majora2007.ctci.chapter1;

/**
 * User: jvmilazz
 * Date: 2/4/14
 * Time: 9:38 AM
 */
public class Mergesort {
    private char[] array;
    private char[] helper;

    private int length;

    public void sort(char[] values) {
        this.array = values;
        length = values.length;
        this.helper = new char[length];
        mergesort(0, length - 1);
    }

    private void mergesort(int low, int high) {
        // check if low is smaller than high, if not then array is sorted
        if (low < high)
        {
            // Get the index of the element in middle
            int middle = low + (high - low) / 2;
            // sor the left side of the array
            mergesort(low, middle);
            // Sort the right side of the array
            mergesort(middle + 1, high);
            // Combine them both
            merge(low, middle, high);
        }
    }

    private void merge(int low, int middle, int high)
    {
        // Copy both parts into the helper array
        for (int i = low; i <= high; i++)
        {
            helper[i] = array[i];
        }

        int i = low;
        int j = middle + 1;
        int k = low;

        // Copy the smallest values from either the left or the right side back to the original array
        while (i <= middle && j <= high) {
            if (new Character(helper[i]).compareTo(new Character(helper[j])) <= 0) {
                array[k] = helper[i];
                i++;
            } else {
                array[k] = helper[j];
                j++;
            }
            k++;
        }

        // Copy the rest of the left side of the array into the target array
        while (i <= middle)
        {
            array[k] = helper[i];
            k++;
            i++;
        }
    }
}
