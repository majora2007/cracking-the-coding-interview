package com.majora2007.ctci;

/**
 * User: jvmilazz
 * Date: 2/11/14
 * Time: 8:53 AM
 */
public class MyStack {

    IntNode head;

    public MyStack() {

    }

    public void push(int i) {
        push(new IntNode(i));
    }

    private void push(IntNode node) {
        if (head == null) {
            head = node;
        } else {
            node.setNext(head);
            head = node;
        }
    }

    public Object pop() {
        if (head == null) {
            return null;
        } else {
            int returnData = head.getData();
            head = head.getNext();
            return returnData;
        }
    }

    public Object peek() {
        if (head == null) return null;
        else return head.getData();
    }

}
