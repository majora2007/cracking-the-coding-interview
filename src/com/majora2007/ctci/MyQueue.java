package com.majora2007.ctci;

/**
 * User: jvmilazz
 * Date: 2/11/14
 * Time: 9:19 AM
 */
public class MyQueue {

    IntNode head;
    IntNode tail;

    public MyQueue() {

    }

    public void enqueue(int i) {
        enqueue(new IntNode(i));
    }

    private void enqueue(IntNode node) {
        if(head == null) {
            tail = node;
            head = tail;
        } else {
            tail.setNext(node);
            tail = tail.getNext();
        }
    }

    public Object dequeue() {
        if (head != null) {
            Object returnData = peek();
            head = head.getNext();
            return returnData;
        }
        return null;
    }

    public Object peek() {
        if (head == null) {
            return null;
        }

        return head.getData();
    }
}
