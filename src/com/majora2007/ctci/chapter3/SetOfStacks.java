package com.majora2007.ctci.chapter3;

import java.util.Stack;
import java.util.Vector;

/**
 * User: jvmilazz
 * Date: 2/11/14
 * Time: 12:40 PM
 */
public class SetOfStacks {

    Vector<Stack<Object>> allStacks;
    private int stackCapacity = 1;

    private int totalItems = 0;
    private int numStacks = 0;

    public SetOfStacks(int initialCapacity, int stackCapacity)
    {
        allStacks = new Vector<Stack<Object>>(initialCapacity);
        this.stackCapacity = stackCapacity;
        for (int i = 0; i < allStacks.size(); i++)
        {
            allStacks.addElement(new Stack<Object>());
            numStacks++;
        }
    }

    public void push(Object item)
    {
        int currentStack = stackMap();

        allStacks.get(currentStack).push(item);
        totalItems++;
    }

    public Object pop() {
        int currentStack = stackMap();

        Stack<Object> current = allStacks.get(currentStack);
        Object result = current.pop();

        totalItems--;

        return result;
    }

    private int stackMap() {
        if (totalItems == 0) return 0;

        return totalItems / stackCapacity;
    }

}
