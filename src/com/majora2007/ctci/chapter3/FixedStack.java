package com.majora2007.ctci.chapter3;

/**
 * User: jvmilazz
 * Date: 2/11/14
 * Time: 10:52 AM
 */

/**
 * This is 3 queues managed through one single array.
 * This provides two implementations:
 * a) Each queue has fixed length
 * b) Queues have flexible length (increased complexity)
 */
public class FixedStack {

    private class StackWrapper {

        private int startIndex, endIndex, head;
        private Object[] stack;

        /**
         * A wrapper which uses a portion of shared array to maintain stack.
         * @param start Inclusive
         * @param end Exclusive
         * @param stackReference
         */
        public StackWrapper(int start, int end, Object[] stackReference) {

            this.startIndex = start;
            this.endIndex = end;
            this.stack = stackReference;

            this.head = endIndex;

            System.out.println("Head for Stack 1 is " + this.head);
        }

        public void push(Object item) {

            if (head == endIndex) {
                stack[head] = item;
            } else if (head == startIndex && stack[head] != null) {
                throw new RuntimeException("Stack is full");
            } else {
                head -= 1;
                stack[head] = item;
            }

            /*if (this.stack[head] == null && head >= startIndex)
            {
                this.stack[head] = item;
                head -= 1;
            } else {
                throw new RuntimeException("Stack is full");
            }*/


        }

        public Object pop() {
            return null;
        }

        public Object peek() {
            if (head < startIndex) return null;

            return this.stack[head];
        }

        public int length() {
            return endIndex - startIndex;
        }

    }

    public static final int STACK_1 = 0, STACK_2 = 1, STACK_3 = 2;

    private Object[] masterStack;
    private StackWrapper stack1, stack2, stack3;

    public FixedStack(int length) throws RuntimeException
    {
        if (length <= 0) throw new RuntimeException("Cannot have 0 length.");

        masterStack = new Object[length * 3];

        for (int i = 0; i < masterStack.length; i++)
        {
            masterStack[i] = null;
        }

        int len = length * 3;
        stack1 = new StackWrapper(0, len/3 - 1, masterStack);
        stack2 = new StackWrapper(len/3, (2*len)/3 - 1, masterStack);
        stack3 = new StackWrapper((2* len)/3, len - 1, masterStack);

        /*System.out.println("Stack 1 Length: " + stack1.length());
        System.out.println("Stack 2 Length: " + stack2.length());
        System.out.println("Stack 3 Length: " + stack3.length());*/
    }

    public void push(int stack, Object item)
    {
        switch(stack)
        {
            case STACK_1:
                stack1.push(item);
                break;
            case STACK_2:
                stack2.push(item);
                break;
            case STACK_3:
                stack3.push(item);
                break;
            default:
                throw new RuntimeException("Not a valid stack.");
        }
    }

    public Object peek(int stack)
    {
        switch(stack)
        {
            case STACK_1:
                return stack1.peek();
            case STACK_2:
                return stack2.peek();
            case STACK_3:
                return stack3.peek();
            default:
                throw new RuntimeException("Not a valid stack.");
        }
    }

}
