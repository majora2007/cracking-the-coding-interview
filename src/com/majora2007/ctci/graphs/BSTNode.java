package com.majora2007.ctci.graphs;

public class BSTNode {
    private int data;
    private BSTNode left, right;

    public BSTNode(int val) {
        this.data = val;
        this.left = null;
        this.right = null;
    }

    public void setLeft(BSTNode left) {
        this.left = left;
    }

    public void setRight(BSTNode right) {
        this.right = right;
    }

    public BSTNode getLeft() {
        return this.left;
    }

    public BSTNode getRight() {
        return this.right;
    }

    public int getData() {
        return this.data;
    }

    public boolean equals(BSTNode other) {
        if (this.data == (int) other.getData()) return true;
        return false;
    }
}
