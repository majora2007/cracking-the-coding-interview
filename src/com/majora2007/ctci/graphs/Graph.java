package com.majora2007.ctci.graphs;

import java.util.*;

/**
 * User: jvmilazz
 * Date: 2/22/14
 * Time: 2:30 PM
 */
public class Graph {
    protected Node rootNode = null;
    protected ArrayList nodes = new ArrayList<Node>();
    protected int[][] adjMatrix; // Edges will be represented as adjacency matrix
    protected int size;

    public Graph() {

    }

    public void setRootNode(Node root)
    {
        this.rootNode = root;
    }

    public Node getRoot() {
        return this.rootNode;
    }

    public void addNode(Node n) {
        nodes.add(n);
    }

    public void connectNode(Node start, Node end)
    {
        if (adjMatrix == null)
        {
            size = nodes.size();
            adjMatrix = new int[size][size];
        }

        int startIndex = nodes.indexOf(start);
        int endIndex = nodes.indexOf(end);
        adjMatrix[startIndex][endIndex] = 1;
        adjMatrix[endIndex][startIndex] = 1;
    }

    /**
     * Breadth First Search is limited to two operations:
     * a) visit and inspect a node of a graph
     * b) gain access to visit the nodes that neighbor the currently visited node
     *
     * BFS begins at a root node and inspects all the neighboring nodes. Then for each of those
     * neighbor nodes in turn, it inspects their neighbor nodes which were unvisited and so on.
     *
     * Worst case performance:          O(|E|)
     * Worst case space complexity:     O(|V|)
     *
     * Uses:
     * Finding all nodes within one connected component
     * Finding shortest path between two nodes u and v (with path length measured by number of edges)
     * Testing a graph for bipartiteness
     * Ford-Fulkerson method for computing the maximum flow in a flow network
     * Searialization/Deserialiszation of a binary tree vs serialization in sorted order
     */
    public void breadthFirstSearch()
    {
        if (rootNode == null) throw new IllegalStateException("Root node has not been set.");

        Queue<Node> queue = new LinkedList<Node>();
        Node root = this.rootNode;

        queue.add(root);
        visit(root);

        while (!queue.isEmpty())
        {
            Node parent = queue.remove();
            Node child = null;
            while ((child = getUnvisitedChildNode(parent)) != null)
            {
                visit(child);
                printNode(child);
                queue.add(child);
            }
        }
    }

    /**
     * Worst case performance: O(|E|) for explicit graph traversal without repetition
     * Worst case space complexity: O(|V|) if entire graph is traversed without repetition,
     *                              O(longest path length searched) for implicit graphs without elimination of duplicate nodes
     */
    public void depthFirstSearch()
    {
        if (this.rootNode == null) throw new IllegalStateException("Root node has not been set.");

        Stack<Node> stack = new Stack<Node>();
        Node root = this.rootNode;

        stack.push(root);
        visit(root);

        while (!stack.isEmpty())
        {
            Node parent = stack.peek();
            Node child = getUnvisitedChildNode(parent);

            if (child != null)
            {
                visit(child);
                printNode(child);
                stack.push(child);
            } else {
                stack.pop();
            }
        }
    }

    protected void visit(Node node)
    {
        node.visited = true;
    }

    public void reset() {
        clearNodes();
    }

    private Node getUnvisitedChildNode(Node node)
    {
        int index = nodes.indexOf(node);
        int j = 0;

        while (j < size)
        {
            if (adjMatrix[index][j] == 1 && ((Node) nodes.get(j)).visited == false)
            {
                return (Node) nodes.get(j);
            }
            j++;
        }

        return null;
    }


    /**
     * Utility method for clearing visited property of nodes
     */
    protected void clearNodes() {
        int i = 0;
        while (i < size)
        {
            Node node = (Node) nodes.get(i);
            node.visited = false;
            i++;
        }
    }

    private void printNode(Node node)
    {
        System.out.print(node.label + " ");
    }

    public void printAdjacencyMatrix()
    {
        System.out.println(Arrays.deepToString(adjMatrix));
    }

    public int[][] getAdjacencyMatrix() // TODO: Make protected
    {
        return this.adjMatrix;
    }
}
