package com.majora2007.ctci.graphs;

public class BinarySearchTree {

    public BinarySearchTree() {
    }

    public void insert(BSTNode root, BSTNode node) {
        if (root == null || node == null) return;

        if (root.getData() > node.getData())
        {
            if (root.getLeft() == null)
            {
                root.setLeft(node);
                System.out.println("Added node to left of " + root.getData() + " of value " + node.getData());
            } else {
                insert(root.getLeft(), node);
            }
        } else if (root.getData() < node.getData())
        {
            if (root.getRight() == null) {
                root.setRight(node);
                System.out.println("Added node to right of " + root.getData() + " of value " + node.getData());
            } else {
                insert(root.getRight(), node);
            }
        }
    }

    //public BSTNode delete()

    public boolean binarySearch(BSTNode root, BSTNode node) {
        if (root == null || node == null) return false;

        System.out.println(" Searching " + root.getData() + " for value " + node.getData());
        if (root.getData() == node.getData())
        {
            return true;
        } else if (root.getData() > node.getData())
        {
            return binarySearch(root.getLeft(), node);
        } else if (root.getData() < node.getData())
        {
            return binarySearch(root.getRight(), node);
        }

        return false;
    }

    public void inOrderTraversal(BSTNode root)
    {
        if (root != null) {
            inOrderTraversal(root.getLeft());
            System.out.println(" " + root.getData());
            inOrderTraversal(root.getRight());
        }
    }

    public void preOrderTraversal(BSTNode root)
    {
        if (root != null) {
            System.out.println(" " + root.getData());
            preOrderTraversal(root.getLeft());
            preOrderTraversal(root.getRight());
        }
    }

    public void postOrderTraversal(BSTNode root)
    {
        if (root != null) {
            postOrderTraversal(root.getLeft());
            postOrderTraversal(root.getRight());
            System.out.println(" " + root.getData());
        }
    }
}
