package com.majora2007.ctci.graphs;

/**
 * User: jvmilazz
 * Date: 2/22/14
 * Time: 2:38 PM
 */
public class Node {
    public String label;
    public boolean visited = false;

    public Node(String label)
    {
        this.label = label;
    }

}
