package com.majora2007.ctci;

/**
 * User: jvmilazz
 * Date: 2/11/14
 * Time: 8:53 AM
 */
public class IntNode {

    private int data;
    private IntNode next;

    public IntNode(int data) {
        this.data = data;
        this.next = null;
    }

    public int getData() {
        return data;
    }

    public void setData(final int data) {
        this.data = data;
    }

    public IntNode getNext() {
        return next;
    }

    public void setNext(final IntNode next) {
        this.next = next;
    }

    public void add(int i)
    {
        add(new IntNode(i));
    }

    public void add(IntNode node) {
        IntNode currentNode = this;

        while (currentNode.getNext() != null)
        {
            currentNode = currentNode.getNext();
        }

        currentNode.setNext(node);
    }

    public IntNode pop() {
        IntNode currentNode = this;
        IntNode prevNode = this;
        while (currentNode.getNext() != null) {
            prevNode = currentNode;
            currentNode = currentNode.getNext();
        }

        prevNode.setNext(null);
        return currentNode;
    }
}
