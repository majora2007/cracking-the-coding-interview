package com.majora2007.ctci.chapter2;

import java.util.HashMap;

/**
 * User: jvmilazz
 * Date: 2/6/14
 * Time: 8:50 AM
 */
public class Chapter2Application {

    public Chapter2Application() {

    }

    // Question 2.5
    public int reverseAdd(Node<Integer> num1, Node<Integer> num2, int multCounter, int sum)
    {
        int n1 = 0, n2 = 0;

        if (num1 == null && num2 == null) return sum;

        if (num1 != null) {
            n1 = num1.getData() * multCounter;
        }

        if (num2 != null) {
            n2 = num2.getData() * multCounter;
        }

        sum += (n1 + n2);

        if (num1 == null) {
            return reverseAdd(num1, num2.getNext(), multCounter *= 10, sum);
        } else if (num2 == null) {
            return reverseAdd(num1.getNext(), num2, multCounter *= 10, sum);
        } else {
            return reverseAdd(num1.getNext(), num2.getNext(), multCounter *= 10, sum);
        }

    }

    // Runtime: O(nm) time; n num of digits of num1, m is num of digitis of num2
    public Node<Integer> reverseListAdd(Node<Integer> num1, Node<Integer> num2, int multCounter, int carryOver)
    {
        int n1 = 0, n2 = 0;
        Node<Integer> outputList = null;

        if (num1 == null && num2 == null) return outputList;

        if (num1 != null) {
            n1 = num1.getData() * multCounter;
        }

        if (num2 != null) {
            n2 = num2.getData() * multCounter;
        }

        //sum += (n1 + n2);
        int sum = n1+n2 + carryOver;
        int lastDigit;
        if (sum >= 10)
        {
            lastDigit = sum % 10;
            carryOver = sum - lastDigit;
        } else {
            lastDigit = sum;
            carryOver = 0;
        }

        if (outputList == null)
        {
            outputList = new Node<Integer>(new Integer(lastDigit));
        } else {
            outputList.append(new Integer(lastDigit));
        }


        if (num1 == null) {
            return reverseListAdd(num1, num2.getNext(), multCounter *= 10, carryOver);
        } else if (num2 == null) {
            return reverseListAdd(num1.getNext(), num2, multCounter *= 10, carryOver);
        } else {
            return reverseListAdd(num1.getNext(), num2.getNext(), multCounter *= 10, carryOver);
        }
    }

    // Question 2.2
    // O(n) time, O(1) space
    public Node<Integer> findKLastElement(Node<Integer> list, int k)
    {
        // Pre-optimization: If k is 0, we can normally iterate through however this extra code not worth maintainability
        Node<Integer> currentNode = list;
        Node<Integer> endNode = list;
        for (int i = 0; i < k; i++)
        {
            endNode = endNode.getNext();
        }

        while (endNode.getNext() != null) {
            currentNode = currentNode.getNext();
            endNode = endNode.getNext();
        }

        return currentNode;
    }

    /**
     * Question 2.4:
     * Write code to partition a linked list around a value x, such that all nodes less than x come before all nodes
     * greater than or equal to x.
     *
     */
    /*public BSTNode<Integer> partitionListAroundValue(BSTNode<Integer> list, int x)
    {
        // Find BSTNode x
        BSTNode<Integer> currNode = list;
        while (currNode.getNext() != null && currNode.getData() != x)
        {
            currNode = currNode.getNext();
        }

        assert(currNode.getData() == x);

        // Partiton list into two sub-lists.

    }*/

    /**
     * Question 2.6:
     * Given a circular linked list, implement an algorithm which returns the node at the beginning of the loop.
     *
     * O(n) time, O(n) space
     * @param head
     * @return
     */
    public Node findFirstNodeInCycle(Node<Character> head) {
        Node<Character> currentNode = head;
        HashMap<Character, Integer> seenNodes = new HashMap<Character, Integer>();

        while (currentNode != null)
        {
            if (seenNodes.containsKey(currentNode.getData()))
            {
                return currentNode;
            } else {
                seenNodes.put(new Character(currentNode.getData()), new Integer(1));
            }

            currentNode = currentNode.getNext();
        }

        return null;
    }





}
