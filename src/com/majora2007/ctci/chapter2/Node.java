package com.majora2007.ctci.chapter2;

/**
 * User: jvmilazz
 * Date: 2/8/14
 * Time: 10:01 AM
 */
public class Node<T> {

    private T data;
    private Node next = null;
    private Node prev = null; // Only use for doublely linked lists

    public Node(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(final T data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(final Node next) {
        this.next = next;
    }

    public Node getPrev() {
        return prev;
    }

    public void setPrev(final Node prev) {
        this.prev = prev;
    }

    public void append(T data) {
        Node end = new Node<T>(data);
        Node n = this;

        while (n.getNext() != null) {
            n = n.getNext();
        }

        n.setNext(end);
    }
}
