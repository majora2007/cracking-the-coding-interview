package com.majora2007.ctci;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * User: jvmilazz
 * Date: 2/11/14
 * Time: 9:06 AM
 */
public class MyStackTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testPush() throws Exception {

        MyStack stack = new MyStack();
        stack.push(0);
        stack.push(1);

        assertEquals(1, stack.pop());
        assertEquals(0, stack.pop());
    }

    @Test
    public void testPop() throws Exception {

        MyStack stack = new MyStack();
        assertEquals(null, stack.pop());

        stack.push(0);
        assertEquals(0, stack.pop());



    }

    @Test
    public void testPeek() throws Exception {

        MyStack stack = new MyStack();
        assertEquals(null, stack.peek());

        stack.push(0);
        assertEquals(0, stack.peek());

    }
}
