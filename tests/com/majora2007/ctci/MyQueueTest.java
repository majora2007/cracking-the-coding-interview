package com.majora2007.ctci;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * User: jvmilazz
 * Date: 2/11/14
 * Time: 9:27 AM
 */
public class MyQueueTest {
    @Test
    public void testEnqueue() throws Exception {

        MyQueue queue = new MyQueue();

        queue.enqueue(0);
        assertEquals(new Integer(0), queue.peek());

        queue.enqueue(1);
        assertEquals(new Integer(0), queue.peek());

    }

    @Test
    public void testDequeue() throws Exception {

        MyQueue queue = new MyQueue();

        queue.enqueue(0);
        queue.enqueue(1);


        assertEquals(0, queue.dequeue());
        assertEquals(new Integer(1), queue.dequeue());
        assertNull(queue.peek());

    }

    @Test
    public void testPeek() throws Exception {
        MyQueue queue = new MyQueue();

        assertEquals(queue.peek(), null);

        queue.enqueue(0);
        assertEquals(new Integer(0), queue.peek());
        queue.enqueue(1);
        assertEquals(new Integer(0), queue.peek());

    }
}
