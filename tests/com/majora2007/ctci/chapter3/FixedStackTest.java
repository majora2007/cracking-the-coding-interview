package com.majora2007.ctci.chapter3;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

/**
 * User: jvmilazz
 * Date: 2/11/14
 * Time: 11:28 AM
 */
public class FixedStackTest {

    FixedStack fixedStack;

    @Before
    public void setUp() {
        fixedStack = new FixedStack(3);
    }

    @Test
    public void testPush() throws Exception {

        /*fixedStack.push(FixedStack.STACK_1, 1);
        Assert.assertEquals(1, fixedStack.peek(FixedStack.STACK_1));
        fixedStack.push(FixedStack.STACK_1, 2);
        Assert.assertEquals(2, fixedStack.peek(FixedStack.STACK_1));
        fixedStack.push(FixedStack.STACK_1, 3);
        Assert.assertEquals(3, fixedStack.peek(FixedStack.STACK_1));
        fixedStack.push(FixedStack.STACK_1, 4);

        Assert.assertEquals(3, fixedStack.peek(FixedStack.STACK_1));*/

    }

    @Test
    public void testPeek() throws Exception {
        fixedStack.push(FixedStack.STACK_1, 1);
        fixedStack.push(FixedStack.STACK_2, 2);
        fixedStack.push(FixedStack.STACK_3, 3);

        Assert.assertEquals(1, fixedStack.peek(FixedStack.STACK_1));
        Assert.assertEquals(2, fixedStack.peek(FixedStack.STACK_2));
        Assert.assertEquals(3, fixedStack.peek(FixedStack.STACK_3));

    }
}
