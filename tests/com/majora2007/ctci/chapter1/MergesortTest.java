package com.majora2007.ctci.chapter1;

import org.junit.Before;
import org.junit.Test;
import java.util.Random;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.fail;


public class MergesortTest {

    private char[] string;
    private final static int SIZE = 7;
    private final static int MIN = 65;
    private final static int MAX = 122;

    @Before
    public void setUp() throws Exception {
        string = new char[SIZE];
        // Create random string
        Random generator = new Random();
        for (int i = 0; i < string.length; i++)
        {
            string[i] = (char) (generator.nextInt(MAX - MIN) + MIN);
        }

    }

    @Test
    public void testSort() throws Exception {
        long startTime = System.currentTimeMillis();

        Mergesort sorter = new Mergesort();
        sorter.sort(string);

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Mergesort " + elapsedTime);

        for (int i = 0; i < string.length - 1; i++) {
            if (new Character(string[i]).compareTo(new Character(string[i+1])) > 0) {
                fail("Should not happen");
            }
        }

        assertTrue(true);
    }
}
