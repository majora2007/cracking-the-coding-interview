package com.majora2007.ctci.chapter1;

import com.majora2007.ctci.chapter1.Chapter1Application;
import com.majora2007.ctci.chapter1.Pixel;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert.*;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class Chapter1ApplicationTest {

    private Chapter1Application app;

    private Pixel[][] originalImage;

    @Before
    public void setUp() throws Exception {
        app = new Chapter1Application();

        originalImage = new Pixel[2][2];
        originalImage[0][0] = new Pixel(127, 0, 0, 0);
        originalImage[0][1] = new Pixel(0, 127, 0, 0);
        originalImage[1][0] = new Pixel(0, 0, 127, 0);
        originalImage[1][1] = new Pixel(0, 0, 0, 127);

        /*for (int i = 0; i < originalImage.length; i++)
        {
            for (int j = 0; j < originalImage[0].length; j++)
            {

            }
        }*/

    }

    @Test
    public void testHasUniqueCharactersWithHash() throws Exception {

        String input = "abcdefg";
        boolean result = app.hasUniqueCharactersWithHash(input);

        assertTrue("hasUniqueCharactersWithHash for input " + input, app.hasUniqueCharactersWithHash(input));
        assertFalse(app.hasUniqueCharactersWithHash("abcdefa"));

    }

    @Test
    public void testIsPermutationOf() throws Exception {
        String stringA = "aabba";
        String stringB = "baaab";

        assertTrue(app.isPermutationOf(stringA, stringB));
        assertFalse(app.isPermutationOf("aabba", "baaac"));

    }

    @Test
    public void testHasUniqueCharacters() throws Exception {

        assertTrue(app.hasUniqueCharacters("abcdefg"));
        assertFalse(app.hasUniqueCharacters("abcdefa"));

    }

    @Test
    public void testCompressString() throws Exception {

        assertEquals("a2b1c5a3", app.compressString("aabcccccaaa"));
    }

    /*@Test
    public void testRotateImageBy90Degrees() throws Exception {

        for (int i = 0; i < originalImage.length; i++)
        {
            for (int j = 0; j < originalImage[0].length; j++)
            {
                System.out.print(originalImage[i][j].toString() + " ");
            }
            System.out.println("");
        }

        System.out.println("Rotated:");
        app.rotateImageBy90Degrees(originalImage);
        for (int i = 0; i < originalImage.length; i++)
        {
            for (int j = 0; j < originalImage[0].length; j++)
            {
                System.out.print(originalImage[i][j].toString() + " ");
            }
            System.out.println("");
        }

    }*/
}
