package com.majora2007.ctci.random;

import junit.framework.Assert;
import org.junit.Test;

/**
 * User: jvmilazz
 * Date: 2/22/14
 * Time: 4:14 PM
 */
public class HeapSortTest {
    @Test
    public void testHeapSort() throws Exception {

        int[] input = new int[] {3, 7, 8, 1, 2, 14, 4};
        int[] correctOutput = new int[] {1, 2, 3, 4, 7, 8, 14};

        HeapSort heapSort = new HeapSort();
        int[] output = heapSort.heapSort(input);

        for (int i = 0; i < output.length; i++)
        {
            Assert.assertEquals(correctOutput[i], output[i]);
        }

    }

    @Test
    public void testSwap() throws Exception {
        int[] input = new int[] {3, 7};
        int[] correctOutput = new int[] {7, 3};

        HeapSort heapSort = new HeapSort();
        heapSort.swap(input, 0, 1);

        for (int i = 0; i < input.length; i++)
        {
            System.out.println("input " + input[i]);
            Assert.assertEquals(correctOutput[i], input[i]);
        }

    }
}
