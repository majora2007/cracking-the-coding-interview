package com.majora2007.ctci.graphs;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * User: jvmilazz
 * Date: 2/22/14
 * Time: 2:49 PM
 */
public class GraphTest {

    Node nA = new Node("A");
    Node nB = new Node("B");
    Node nC = new Node("C");
    Node nD = new Node("D");
    Node nE = new Node("E");
    Node nF = new Node("F");

    Graph graph;

    @Before
    public void setUp() throws Exception {
        graph = new Graph();
        graph.addNode(nA);
        graph.addNode(nB);
        graph.addNode(nC);
        graph.addNode(nD);
        graph.addNode(nE);
        graph.addNode(nF);
        graph.setRootNode(nA);

        graph.connectNode(nA, nB);
        graph.connectNode(nA, nC);
        graph.connectNode(nA, nD);

        graph.connectNode(nB, nE);
        graph.connectNode(nB, nF);
        graph.connectNode(nC, nF);

        System.out.println("Starting Adjacency Matrix:");
        graph.printAdjacencyMatrix();
    }

    @After
    public void tearDown() throws Exception {

        //System.out.println("Ending Adjacency Matrix:");
        //graph.printAdjacencyMatrix();

        graph.reset();

    }

    @Test
    public void testConnectNode() throws Exception {

    }

    @Test
    public void testBreadthFirstSearch() throws Exception {
        System.out.println("BFS Traversal of a tree is: ");
        graph.breadthFirstSearch();


    }

    @Test
    public void testDepthFirstSearch() throws Exception {
        System.out.println("DFS Traversal of a tree is: ");
        graph.depthFirstSearch();
    }
}
