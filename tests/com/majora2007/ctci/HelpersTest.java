package com.majora2007.ctci;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * User: jvmilazz
 * Date: 2/8/14
 * Time: 11:22 AM
 */
public class HelpersTest {
    @Test
    public void testCompareBinaryToHex() throws Exception {
        assertTrue(Helpers.compareBinaryToHex("1001", "09"));
    }

    @Test
    public void testConvertToBase() throws Exception {

        assertEquals(1, Helpers.convertToBase("1", 2));
        assertEquals(0, Helpers.convertToBase("0", 2));

        assertEquals(0, Helpers.convertToBase("0", 16));
        assertEquals(9, Helpers.convertToBase("9", 16));

    }

    @Test
    public void testDigitToValue() throws Exception {

        assertEquals(9, Helpers.digitToValue('9'));
        assertEquals(10, Helpers.digitToValue('A'));

    }
}
