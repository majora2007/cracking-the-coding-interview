package com.majora2007.ctci.chapter2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * User: jvmilazz
 * Date: 2/8/14
 * Time: 1:18 PM
 */
public class Chapter2ApplicationTest {

    Chapter2Application app;

    //BSTNode<Integer> number1;
    //BSTNode<Integer> number2;

    Node<Integer> number1;
    Node<Integer> number2;

    @Before
    public void setUp() throws Exception {

        app = new Chapter2Application();

        number1 = new Node<Integer>(7);
        number1.append(new Integer(1));
        //number1.append(new Integer(6));

        number2 = new Node<Integer>(new Integer(5));
        number2.append(new Integer(9));
        number2.append(new Integer(2));
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testReverseAdd() throws Exception {

        number1 = new Node<Integer>(7);
        number1.append(new Integer(1));
        number1.append(new Integer(6));

        number2 = new Node<Integer>(new Integer(5));
        number2.append(new Integer(9));
        number2.append(new Integer(2));

        assertTrue(app.reverseAdd(number1, number2, 1, 0) == 912);

        number1 = new Node<Integer>(7);
        number1.append(new Integer(1));
        //number1.append(new Integer(6));

        number2 = new Node<Integer>(new Integer(5));
        number2.append(new Integer(9));
        number2.append(new Integer(2));
        assertTrue(app.reverseAdd(number1, number2, 1, 0) == 312);

        number1 = new Node<Integer>(7);
        number1.append(new Integer(1));
        //number1.append(new Integer(6));

        number2 = new Node<Integer>(new Integer(5));
        number2.append(new Integer(9));
        number2.append(new Integer(2));
        assertTrue(app.reverseAdd(number1, number2, 1, 0) == 312);
    }

    @Test
    public void testreverseListAdd() throws Exception {
        number1 = new Node<Integer>(7);
        number1.append(new Integer(1));
        number1.append(new Integer(6));

        number2 = new Node<Integer>(new Integer(5));
        number2.append(new Integer(9));
        number2.append(new Integer(2));

        Node<Integer> expectedOutput = new Node<Integer>(new Integer(9));
        expectedOutput.append(new Integer(1));
        expectedOutput.append(new Integer(2));

        Node<Integer> output = app.reverseListAdd(number1, number2, 1, 0);

        Node<Integer> expectNode = expectedOutput;
        Node<Integer> outputNode = output;

        while (outputNode != null && expectedOutput != null)
        {
            assertEquals(expectNode.getData(), outputNode.getData());
            expectNode = expectNode.getNext();
            outputNode = outputNode.getNext();
        }
    }

    @Test
    public void testFindKLastElement() throws Exception {
        Node<Integer> inputList = new Node<Integer>(new Integer(0));
        for (int i = 1; i < 5; i++)
        {
            inputList.append(new Integer(i));
        }

        Node<Integer> expectedOutput = new Node<Integer>(new Integer(2));
        Node<Integer> output = app.findKLastElement(inputList, 2);

        assertEquals(expectedOutput.getData(), output.getData());
    }

    @Test
    public void testFindFirstNodeInCycle() throws Exception {
        Node<Character> inputList = new Node<Character>(new Character('A'));
        inputList.append(new Character('B'));
        inputList.append(new Character('C'));
        inputList.append(new Character('D'));
        inputList.append(new Character('E'));

        Node<Character> currNode = inputList;
        Node<Character> cNode = null;
        while (currNode.getNext() != null)
        {
            System.out.print(currNode.getData() + "->");
            if (currNode.getData().equals(new Character('C')))
            {
                cNode = currNode;
            }

            currNode = currNode.getNext();

        }

        currNode.setNext(cNode);

        System.out.println("Found node: " + app.findFirstNodeInCycle(inputList).getData());

        assertEquals(cNode, app.findFirstNodeInCycle(inputList));
        //assertEquals(app.findFirstNodeInCycle(inputList), null);


    }
}
